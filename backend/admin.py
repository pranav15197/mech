from django.contrib import admin
from backend.models import *
from nested_inline.admin import NestedStackedInline, NestedModelAdmin

admin.site.register(Group)
admin.site.register(Area_of_interest)
admin.site.register(Field)
admin.site.register(SubField)
class customAdmin(NestedModelAdmin):
    exclude=['created_by']
    def get_readonly_fields(self, request, obj=None):
        if obj: 
            return self.readonly_fields+('created_by',)
        return self.readonly_fields
    
    def get_form(self, request, obj=None, **kwargs):
     # Proper kwargs are form, fields, exclude, formfield_callback
        if obj == None: # obj is  None, so this is a add page
            kwargs['exclude'] = ['created_by',]
        return super(customAdmin, self).get_form(request, obj, **kwargs)

    def save_model(self, request, obj, form, change):
            if getattr(obj, 'created_by', None) is None:
                obj.created_by = request.user
            obj.save()

    def queryset(self, request):
            """Limit Pages to those that belong to the request's user."""
            qs = super(customAdmin, self).get_queryset(request)
            if request.user.is_superuser:
                # It is mine, all mine. Just return everything.
                return qs
            # Now we just add an extra filter on the queryset and
            # we're done. Assumption: Page.owner is a foreignkey
            # to a User.
            return qs.filter(created_by=request.user)
    

admin.site.register(Event,customAdmin)

class QualificationInline(NestedStackedInline):
    model = Qualification
    extra = 1
    
class AwardInline(NestedStackedInline):
    model = Award
    extra = 1
    
class ResearchInline(NestedStackedInline):
    model = Research
    extra = 1

class FacilityInline(NestedStackedInline):
    model = Facility
    extra = 1

class LabImageInline(NestedStackedInline):
    model = LabImage
    extra = 1

class LabCourseInline(NestedStackedInline):
    model = LabCourse
    extra = 1

class CourseInline(NestedStackedInline):
    model = Course
    extra = 1

class BookInline(NestedStackedInline):
    model = Book
    extra = 1

class BookChapInline(NestedStackedInline):
    model = Book_Chapter
    extra = 1

class WorkshopInline(NestedStackedInline):
    model = Workshop
    extra = 1

class PublicationInline(NestedStackedInline):
    model = Publication
    extra = 1
                                
class LabInline(NestedStackedInline):
    model = Lab
    inlines = [FacilityInline,LabImageInline,LabCourseInline]
    extra = 1
    
        
class ProfileAdmin(NestedModelAdmin):
    inlines= [QualificationInline,AwardInline,ResearchInline,LabInline,CourseInline,BookInline,BookChapInline,PublicationInline,WorkshopInline]
    filter_horizontal = ('areas_of_interest',)
    def queryset(self, request):
            """Limit Pages to those that belong to the request's user."""
            qs = super(ProfileAdmin, self).get_queryset(request)
            if request.user.is_superuser:
                # It is mine, all mine. Just return everything.
                return qs
            # Now we just add an extra filter on the queryset and
            # we're done. Assumption: Page.owner is a foreignkey
            # to a User.
            return qs.filter(user=request.user)
                
admin.site.register(Profile,ProfileAdmin)
admin.site.register(News,customAdmin)
