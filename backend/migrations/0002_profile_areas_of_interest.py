# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='areas_of_interest',
            field=models.ManyToManyField(to='backend.Area_of_interest', null=True, blank=True),
            preserve_default=True,
        ),
    ]
