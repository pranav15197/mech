# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Area_of_interest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Award',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('recognization', models.CharField(default=b'Award', max_length=30, choices=[(b'Award', b'Award'), (b'Fellowship', b'Fellowship')])),
                ('award_name', models.CharField(max_length=100, null=True)),
                ('year', models.IntegerField(null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('year', models.IntegerField(null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Book_Chapter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('book_name', models.CharField(max_length=100)),
                ('year', models.IntegerField(null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Common_Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('project_title', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=400)),
                ('expectation', models.CharField(max_length=400)),
                ('year_of_student_applying', models.CharField(default=b'1st UG', max_length=30, choices=[(b'1st UG', b'1st UG'), (b'2st UG', b'2st UG'), (b'3st UG', b'3st UG'), (b'4st UG', b'4st UG'), (b'5st UG', b'5st UG'), (b'1st Mtech', b'1st Mtech'), (b'2st Mtech', b'2st Mtech')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('status', models.CharField(default=b'taught', max_length=30, choices=[(b'taught', b'taught'), (b'ongoing', b'ongoing')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DDP',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('project_title', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=400)),
                ('expectation', models.CharField(max_length=400)),
                ('n_students', models.IntegerField(null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=100)),
                ('location', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=100)),
                ('Date', models.DateField(null=True)),
                ('Time', models.TimeField(null=True)),
                ('created_by', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Facility',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Field',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Lab',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Title', models.CharField(max_length=100, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LabCourse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('lab', models.ForeignKey(to='backend.Lab')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LabImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.FileField(max_length=20000, null=True, upload_to=b'research_images', blank=True)),
                ('lab', models.ForeignKey(to='backend.Lab')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=100)),
                ('name', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PHD_Supervised',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('topic', models.CharField(max_length=100)),
                ('Scholar_Name', models.CharField(max_length=400)),
                ('status', models.CharField(default=b'completed', max_length=30, choices=[(b'active', b'active'), (b'completed', b'completed')])),
                ('registeration_year', models.IntegerField(null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100, blank=True)),
                ('image', models.FileField(max_length=20000, null=True, upload_to=b'profile_images', blank=True)),
                ('post', models.CharField(default=b'Professor', max_length=30, choices=[(b'Professor', b'professor'), (b'Assistant Prof.', b'Assistant Prof.')])),
                ('Building', models.CharField(default=b'building a', max_length=30, choices=[(b'building a', b'building a'), (b'building b', b'building b'), (b'building c', b'building c')])),
                ('Room_No', models.CharField(max_length=10, null=True, blank=True)),
                ('Phone_No', models.IntegerField(null=True, blank=True)),
                ('Fax_No', models.IntegerField(null=True, blank=True)),
                ('email', models.CharField(max_length=40, null=True, blank=True)),
                ('personal_home_page', models.CharField(max_length=200, null=True, blank=True)),
                ('Affliation', models.CharField(default=b'None', max_length=200, choices=[(b'None', b'None'), (b'National Center For Aerospace Innovation and Research (NCAIR)', b'National Center For Aerospace Innovation and Research (NCAIR)'), (b'BiTec', b'BiTec'), (b'OrthoCAD Network Research Facility', b'OrthoCAD Network Research Facility'), (b'Thermal Hydraulics Test Facility', b'Thermal Hydraulics Test Facility'), (b'Suman Mashruwala Advanced Microengineering Laboratory', b'Suman Mashruwala Advanced Microengineering Laboratory')])),
                ('Group', models.ForeignKey(blank=True, to='backend.Group', null=True)),
                ('user', models.OneToOneField(null=True, to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Publication',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('year', models.IntegerField(null=True)),
                ('published_in', models.CharField(default=b'Refereed Journals', max_length=30, choices=[(b'Refereed Journals', b'Refereed Journals'), (b'Conference Proceedings', b'Conference Proceedings')])),
                ('profile', models.ForeignKey(to='backend.Profile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Qualification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('degree', models.CharField(max_length=100, null=True)),
                ('University', models.CharField(max_length=200, null=True)),
                ('Year', models.IntegerField(null=True)),
                ('profile', models.ForeignKey(to='backend.Profile', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Research',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Title', models.CharField(max_length=100, null=True)),
                ('video_link', models.CharField(max_length=300, null=True)),
                ('image', models.FileField(max_length=20000, null=True, upload_to=b'research_images', blank=True)),
                ('ppt', models.FileField(max_length=20000, null=True, upload_to=b'research_ppt', blank=True)),
                ('field', models.ForeignKey(to='backend.Field', null=True)),
                ('profile', models.ForeignKey(to='backend.Profile', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SubField',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('field', models.ForeignKey(to='backend.Field', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Workshop',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('details', models.CharField(max_length=400)),
                ('place', models.CharField(max_length=400)),
                ('profile', models.ForeignKey(to='backend.Profile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='research',
            name='sub_field',
            field=models.ForeignKey(to='backend.SubField', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='phd_supervised',
            name='profile',
            field=models.ForeignKey(to='backend.Profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='news',
            name='profile',
            field=models.ForeignKey(to='backend.Profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='lab',
            name='profile',
            field=models.ForeignKey(to='backend.Profile', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='facility',
            name='lab',
            field=models.ForeignKey(to='backend.Lab'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ddp',
            name='profile',
            field=models.ForeignKey(to='backend.Profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='course',
            name='profile',
            field=models.ForeignKey(to='backend.Profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='common_project',
            name='profile',
            field=models.ForeignKey(to='backend.Profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='book_chapter',
            name='profile',
            field=models.ForeignKey(to='backend.Profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='book',
            name='profile',
            field=models.ForeignKey(to='backend.Profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='award',
            name='profile',
            field=models.ForeignKey(to='backend.Profile', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='area_of_interest',
            name='group',
            field=models.ForeignKey(to='backend.Group', null=True),
            preserve_default=True,
        ),
    ]
