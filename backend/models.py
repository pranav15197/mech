from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Group(models.Model):
    name = models.CharField(max_length=100)
    def __unicode__(self):          
        return self.name

class Field(models.Model):
    name = models.CharField(max_length=100)
    def __unicode__(self):          
        return self.name
        

class SubField(models.Model):
    name = models.CharField(max_length=100)
    field = models.ForeignKey(Field,null=True)
    def __unicode__(self):          
        return self.name
        

class Area_of_interest(models.Model):
    name = models.CharField(max_length=100)
    group = models.ForeignKey(Group,null=True)
    def __unicode__(self):          
        return self.name
    

class Profile(models.Model):    
    _id = models.AutoField(primary_key=True)    
    user = models.OneToOneField(User, on_delete=models.CASCADE,null=True)
    name = models.CharField(max_length=100,blank=True)
    image = models.FileField (upload_to = "profile_images", max_length=20000, blank=True, null=True)

    post = models.CharField(choices = (
                                    ("Professor", ("professor")),
                                    ("Assistant Prof.", ("Assistant Prof.")),
                                )
                                 ,default="Professor",max_length=30)
                                 
    Group = models.ForeignKey(Group,null=True,blank=True)
    areas_of_interest = models.ManyToManyField(Area_of_interest,null=True,blank=True)
    
    Building = models.CharField(choices = (
                ("building a","building a"),
                ("building b","building b"),
                ("building c","building c"),
    ),default ="building a",max_length=30 )
    
    Room_No = models.CharField(max_length=10,null=True,blank=True)
    Phone_No = models.IntegerField(null=True,blank=True)
    Fax_No = models.IntegerField(null =True,blank=True)
    email = models.CharField(max_length=40,null=True,blank=True)
    personal_home_page = models.CharField(max_length = 200,null=True,blank=True)
    Affliation = models.CharField(
        choices = (
            ("None",("None")),
            ("National Center For Aerospace Innovation and Research (NCAIR)",("National Center For Aerospace Innovation and Research (NCAIR)")),
            ("BiTec",("BiTec")),
            ("OrthoCAD Network Research Facility",("OrthoCAD Network Research Facility")),
            ("Thermal Hydraulics Test Facility",("Thermal Hydraulics Test Facility")),
            ("Suman Mashruwala Advanced Microengineering Laboratory",("Suman Mashruwala Advanced Microengineering Laboratory")),
        ),default = "None",max_length=200
    )
    def __unicode__(self):          
        return self.name
        
class Qualification(models.Model):
    degree = models.CharField(max_length=100,null=True);
    profile = models.ForeignKey(Profile,null=True)
    University = models.CharField(max_length=200,null=True);
    Year = models.IntegerField(null=True);
    def __unicode__(self):          
        return self.degree

class Award(models.Model):
    _id = models.AutoField(primary_key=True)
    recognization = models.CharField(choices = (
                                    ("Award", ("Award")),
                                    ("Fellowship", ("Fellowship")),
                                )
                                 ,default="Award",max_length=30)
    award_name = models.CharField(max_length=100,null=True);
    profile = models.ForeignKey(Profile,null=True)
    year = models.IntegerField(null=True);
    
    def __unicode__(self):          
        return self.award_name
        
        
class Research(models.Model):
    field = models.ForeignKey(Field,null=True)
    sub_field = models.ForeignKey(SubField,null=True)
    profile = models.ForeignKey(Profile,null=True)
    Title = models.CharField(max_length=100,null=True)
    video_link = models.CharField(max_length=300,null=True)
    image = models.FileField (upload_to = "research_images", max_length=20000, blank=True, null=True)
    ppt = models.FileField (upload_to = "research_ppt", max_length=20000, blank=True, null=True)
           
        
class Lab(models.Model):
    Title = models.CharField(max_length = 100,null=True)
    profile = models.ForeignKey(Profile,null=True)
    
               
class Facility(models.Model):
    name = models.CharField(max_length=100)
    def __unicode__(self):          
        return self.name   
    lab = models.ForeignKey(Lab)
           
class LabImage(models.Model):
    image = models.FileField (upload_to = "research_images", max_length=20000, blank=True, null=True)
    lab = models.ForeignKey(Lab)
           
class LabCourse(models.Model):
    name = models.CharField(max_length=100)
    def __unicode__(self):          
        return self.name   
    lab = models.ForeignKey(Lab)
        
class Course(models.Model):
    name = models.CharField(max_length=100)
    def __unicode__(self):          
        return self.name   
    profile = models.ForeignKey(Profile)
    status = models.CharField(choices = (
                                    ("taught", ("taught")),
                                    ("ongoing", ("ongoing")),
                                )
                                 ,default="taught",max_length=30)
        
class Workshop(models.Model):
    title = models.CharField(max_length=100)
    def __unicode__(self):          
        return self.title
    details = models.CharField(max_length=400)   
    place = models.CharField(max_length=400)   
    profile = models.ForeignKey(Profile)
            
class Book(models.Model):
    name = models.CharField(max_length=100)
    year = models.IntegerField(null=True)
    def __unicode__(self):          
        return self.name   
    profile = models.ForeignKey(Profile)   

            
class Event(models.Model):
    created_by = models.ForeignKey(User)
    description = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    Date = models.DateField(null=True)
    Time = models.TimeField(null=True)
            
    def __unicode__(self):          
        return self.name   
        
            
class News(models.Model):
     description = models.CharField(max_length=100)
     name = models.CharField(max_length=100)
     def __unicode__(self):          
         return self.name   
     created_by = models.ForeignKey(User,null=True)
             
class Book_Chapter(models.Model):
    name = models.CharField(max_length=100)
    book_name = models.CharField(max_length=100)
    year = models.IntegerField(null=True)
    def __unicode__(self):          
        return self.name   
    profile = models.ForeignKey(Profile) 
               
             
class DDP(models.Model):
    project_title = models.CharField(max_length=100)
    description = models.CharField(max_length=400)
    expectation = models.CharField(max_length=400)
    n_students = models.IntegerField(null=True)
    def __unicode__(self):          
        return self.project_title  
    profile = models.ForeignKey(Profile) 

             
class Common_Project(models.Model):
    project_title = models.CharField(max_length=100)
    description = models.CharField(max_length=400)
    expectation = models.CharField(max_length=400)
    year_of_student_applying = models.CharField(choices = (
                                    ("1st UG", ("1st UG")),
                                    ("2st UG", ("2st UG")),
                                    ("3st UG", ("3st UG")),
                                    ("4st UG", ("4st UG")),
                                    ("5st UG", ("5st UG")),
                                    ("1st Mtech", ("1st Mtech")),
                                    ("2st Mtech", ("2st Mtech")),
                                )
                                 ,default="1st UG",max_length=30)
                                 
    def __unicode__(self):          
        return self.project_title  
    profile = models.ForeignKey(Profile) 
             
class PHD_Supervised(models.Model):
    topic = models.CharField(max_length=100)
    Scholar_Name = models.CharField(max_length=400)

    status = models.CharField(choices = (
                                    ("active", ("active")),
                                    ("completed", ("completed")),
                                )
                                 ,default="completed",max_length=30)
    registeration_year = models.IntegerField(null=True)
    def __unicode__(self):          
        return self.topic 
    profile = models.ForeignKey(Profile) 
               
class Publication(models.Model):
    name = models.CharField(max_length=100)
    year = models.IntegerField(null=True)
    def __unicode__(self):          
        return self.name   
    profile = models.ForeignKey(Profile)
    published_in = models.CharField(choices = (
                                    ("Refereed Journals", ("Refereed Journals")),
                                    ("Conference Proceedings", ("Conference Proceedings")),
                                )
                                 ,default="Refereed Journals",max_length=30)
       
